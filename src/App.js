import React, { Component } from 'react';

import ToDoList from './components/ToDoList';
import './App.css';
import ToDo from "./components/ToDo";


class App extends Component {
    state = {
        inputText: '',
        toDo: []
    };

  changeInput = (value) => {
      this.setState({inputText: value})
  };

  addClick = () => {
    if(this.state.inputText !== '') {
        let toDoArray = [...this.state.toDo, this.state.inputText];
        this.setState({toDo: toDoArray, inputText: ''});
    } else {
        alert('Fill the field!');
    }

  };

  removeHandler = (id) => {
        let toDoArray = [...this.state.toDo];
        toDoArray.splice(id, 1);
        this.setState({toDo: toDoArray})
  };

  render() {
    return (
      <div className="container">
           <h1 className='ToDoList'>To Do List!</h1>
          <ToDoList addClick={this.addClick} newText={this.state.inputText} changed={this.changeInput}>
              {this.state.toDo.map((task, key) => {
                  return <ToDo key={key} toDo={task} id={key} remove={this.removeHandler}/>
              })
              }
          </ToDoList>
      </div>
    );
  }
}

export default App;
