import React from "react";

const ToDo = (props) => <div className='toDo'>
    <span>{props.toDo}</span>
    <button className='removeText' onClick={()=> props.remove(props.id)}>x</button>
</div>;

    export default ToDo;