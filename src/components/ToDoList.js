import React from 'react';

const ToDoList = props => (
    <div className="toDoList">
            <input className='newText' type="text" value={props.newText} onChange={(e) => props.changed(e.target.value)} />
            <button className='add' onClick={props.addClick}>Add</button>
        <div>
            {props.children}
        </div>
    </div>
);

export default ToDoList;